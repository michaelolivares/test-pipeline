import React from 'react';
import 'react-toastify/dist/ReactToastify.min.css';
import SideBar from './components/layout/SideBar';
import { BrowserRouter as Router } from 'react-router-dom';
import { ToastContainer } from "react-toastify";
import { Switch } from "react-router-dom";
import { Route } from "react-router-dom";
import { Redirect } from "react-router-dom";
import AuthState from './context/auth/authState';
import LoginPage from "./pages/Login.page";
import SignUpPage from "./pages/SingUp.page";
import ListUsersPage from "./pages/ListUsers.page";

function App() {
  return (
    <AuthState>
      <Router>
        <SideBar />
        <ToastContainer />
        <Switch>
          <Route path="/login" exact>
            <LoginPage />
          </Route>
          <Route path="/crear" exact>
            <SignUpPage />
          </Route>
          <Route path="/usuarios" exact>
            <ListUsersPage />
          </Route>
          <Redirect to="/login" />
        </Switch>
      </Router>
    </AuthState>
  );
}

export default App;
