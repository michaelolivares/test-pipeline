import React from "react";
import { useContext } from "react";
import { useForm, SubmitHandler} from "react-hook-form";

import AuthContext from "../../context/auth/authContext";



type Inputs = {
  email: string;
  password: string;
  confirmPassword: string;
}

const SignUp = () => { 
  const authContext  = useContext(AuthContext);
  const { signup } = authContext;
  const { register, handleSubmit, formState: { errors }, setValue, getValues} = useForm<Inputs>();
  const onSubmit: SubmitHandler<Inputs> = (data) => {
    signup(data)
    setValue("email","");
    setValue("password","");
    setValue("confirmPassword","");
  }
  return (
    <div className="formulario__usuario"> 
      <div className="container__formulario">
        <h1>Registrate</h1> 
        <form onSubmit={handleSubmit(onSubmit)}>
          <div className="container__formulario-campo">
            <label htmlFor="email">Correo electronico</label>
            <input id="email" type="email" placeholder="correo@correo.com" {...register("email", { required: true })}  />
          </div>
          {errors.email && <span className="alert--error">Este campo es requerido.</span>}
          <div className="container__formulario-campo">
            <label htmlFor="password">Contraseña</label>
            <input id="password" type="password" placeholder="*****"  {...register("password", { required: true })} />
          </div>  
          {errors.password && <span className="alert--error">Este campo es requerido.</span>}
          <div className="container__formulario-campo">
            <label htmlFor="password-confirm">Confirmar contraseña</label>
            <input id="password-confirm" type="password" placeholder="*****"  {...register("confirmPassword", { required: true, validate: () => getValues("confirmPassword") === getValues("password")})} />
          </div>
          {errors.confirmPassword && <span className="alert--error">Las contrasñas deben coincidir.</span>}
          <div className="container__formulario-campo">
            <input className="btn btn--primario btn--block" type="submit" value="Crea usuario"/>
          </div>
        </form>
      </div>
    </div>
  )
}

export default SignUp;
