import React from "react";
import { useContext } from "react";
import { useForm, SubmitHandler} from "react-hook-form";

import AuthContext from "../../context/auth/authContext";

type Inputs = {
  email: string;
  password: string;
}

const Login = () => { 
  const authContext  = useContext(AuthContext);
  const { login } = authContext;
  const { register, handleSubmit, formState: { errors }, setValue} = useForm<Inputs>();
  const onSubmit: SubmitHandler<Inputs> = (data) => {
    login(data)
    setValue("email","");
    setValue("password","");
  }
  return (
    <div className="formulario__usuario"> 
      <div className="container__formulario">
        <h1>Login</h1> 
        <form onSubmit={handleSubmit(onSubmit)}>
          <div className="container__formulario-campo">
            <label htmlFor="email">Correo electronico</label>
            <input id="email" type="email" placeholder="correo@correo.com" {...register("email", { required: true })}  />
          </div>
          {errors.email && <span className="alert--error">Este campo es requerido</span>}
          <div className="container__formulario-campo">
            <label htmlFor="password">Contraseña</label>
            <input id="password" type="password" placeholder="*****"  {...register("password", { required: true })} />
          </div>  
          {errors.password && <span className="alert--error">Este campo es requerido</span>}
          <div className="container__formulario-campo">
            <input className="btn btn--primario btn--block" type="submit" value="Login"/>
          </div>
        </form>
      </div>
    </div>
  )
}

export default Login;
