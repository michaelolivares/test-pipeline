import React from "react";
type IProps = {
  mensaje: string
}

const NotificationError = (props: IProps) => {
  return (
    <div className="notification-toast">
      <div>
        {props.mensaje}
      </div>
    </div>
  )
}

export default NotificationError