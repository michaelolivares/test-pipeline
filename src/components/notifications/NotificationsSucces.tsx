import React from "react";

type IProps = {
  mensaje: string
}

const NotificationSuccess = (props: IProps) => {
  return (
    <div className="notification-toast">
      <div>
        {props.mensaje}
      </div>
    </div>
  )
}

export default NotificationSuccess