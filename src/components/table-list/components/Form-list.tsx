import React from "react";
import { useContext } from "react";
import { useForm } from "react-hook-form";
import { SubmitHandler } from "react-hook-form";

import AuthContext from "../../../context/auth/authContext";

type Inputs = {
  page: number;
}

const FormList = () => {
  const authContext  = useContext(AuthContext);
  const { listUser } = authContext;
  const { register, handleSubmit, formState: { errors} } = useForm<Inputs>();

  const onSubmit: SubmitHandler<Inputs> = (data) => {
    listUser(data.page);
  }

  return (
    <div className="m-10">
      <form onSubmit={handleSubmit(onSubmit)}>
        <div className="container__formulario-campo">
          <label htmlFor="page">Pagina a mostrar</label>
          <input id="page" type="text" placeholder="1" {...register("page", { required: true })}  />
        </div>
        {errors.page && <span className="alert--error">Este campo es requerido</span>}
        <div className="container__formulario-campo">
          <input className="btn btn--primario btn--block" type="submit" value="Buscar"/>
        </div>
      </form>
    </div>
  )
}
export default FormList;