import React from "react";
import { useContext } from "react";
import { Column } from "react-table";

import AuthContext from "../../context/auth/authContext";
import Table from "../table/Table";
import Form from "./components/Form-list";

const TableList = () => {
  const authContext = useContext(AuthContext);
  const { listUsers } = authContext;
  const { data } = listUsers;

  const columns: Array<Column<IUserList>> = [
    { Header: "ID", accessor: "id"},
    { Header: "avatar", accessor: (row: any) => (
      <div className="icon__container">
        <img src={row.avatar} alt={`Avatar_${row.first_name}`} className="icon__container--user" />
      </div>
    )},
    { Header: "Nombre", accessor: "first_name"},
    { Header: "Apellido", accessor: "last_name"},
    { Header: "Correo Electronico", accessor: "email"},
  ]
  return (
    <div className="formulario__usuario">
      <div className="container__table">
        <Form />
        {data.length > 0 ? (
            <Table columns={columns} data={data} />
        ) : (
          <div className="container__table--empty">
            No hay contenido para mostrar
          </div>
        )}
      </div>
    </div>
  )
}
export default TableList;