import React from "react";
import { useTable } from "react-table";
import { Column } from "react-table";

interface ITable {
  columns: Array<Column<any>>;
  data: Array<any>
}

const Table = (props: ITable) => {
  const { columns, data } = props;
  const { 
    getTableProps, 
    getTableBodyProps, 
    headerGroups,
    rows,
    prepareRow 
  } = useTable({
    columns,
    data
  });
  return (
    <table {...getTableProps()} >
      <thead>
        {headerGroups.map((headerGroup, i) => (
          <tr  {...headerGroup.getHeaderGroupProps()} >
            {headerGroup.headers.map((column) => (
              <th className="container__table--item" {...column.getHeaderProps()}>
                <div className="container__table--header">
                  {column.render('Header')}  
                </div>
              </th>
            ))}
          </tr>
        ))}
      </thead>
      <tbody {...getTableBodyProps()} >
        {rows.map((row, i) => {
          prepareRow(row)
          return (
            <tr className="table__row" key={`row_${i}`}>
              {row.cells.map((cell) => {
                return (
                  <td className="container__table--item" {...cell.getCellProps()} >
                    <div>
                      {cell.render('Cell')}
                    </div>
                  </td>
                )
              })}
            </tr>
          )
        })}
      </tbody>
    </table>
  )
}
export default Table;