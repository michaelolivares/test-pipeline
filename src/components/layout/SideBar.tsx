import React from "react";
import { Link} from "react-router-dom";
import { useLocation } from "react-router-dom";

const SideBar = () => {
  const location = useLocation();
  const activeClass = (route:string) => {
    return location.pathname === route ? "active" : null
  }
  return (
    <aside className="sidebar">
      <nav className="nav">
        <ul>
          <li className={`${activeClass("/login")}`}>
            <Link to="/login">
              Login
            </Link>
          </li>
          <li className={`${activeClass("/crear")}`}>
            <Link to="/crear">
              Crear Usuario
            </Link>
          </li>
          <li className={`${activeClass("/usuarios")}`}>
            <Link to="/usuarios">
              Listar usuarios
            </Link>
          </li>
        </ul>
      </nav>
    </aside>
  )
}

export default SideBar;