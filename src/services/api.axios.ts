import axios from 'axios';
import Config from "../config/config";
import { useLogin } from "../hooks/useLogin";

export const ClientAxios = axios.create();

ClientAxios.interceptors.request.use((config) => {
  const { isLogin, credentials } = useLogin();
  if(isLogin) {
    config.headers.Authorization = `${credentials?.token}`
  }
  const baseURL = Config.api.hostname;
  return { baseURL, ...config}
})