export const apiRoutes = {
  auth: {
    login: () => "/api/login",
    signup: () => "/api/register"
  },
  list: {
    list: (id:number) => `/api/users?page=${id}`
  }
}