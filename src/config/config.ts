const Config = {
  api: {
    hostname: process.env.REACT_APP_HOSTNAME_URL,
  }
}

export default Config;
