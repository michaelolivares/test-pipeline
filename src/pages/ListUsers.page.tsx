import React from "react";

import TableComponent from "../components/table-list/table-list";

const ListUsersPage = () => {
  
  return (
    <div className="container">
      <TableComponent />
    </div>
  )
}
export default ListUsersPage;