import React from 'react';
import SignUpComponent from '../components/auth/SignUp';

const SignUpPage = () => {
  return (
    <div className="container">
      <SignUpComponent />
    </div>
  )
}
export default SignUpPage