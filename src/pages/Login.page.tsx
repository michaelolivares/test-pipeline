import React from 'react';
import LoginComponent from '../components/auth/login';

const LoginPage = () => {
  return (
    <div className="container">
      <LoginComponent />
    </div>
  )
}
export default LoginPage