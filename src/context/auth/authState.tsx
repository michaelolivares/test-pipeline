import React from "react";
import { useReducer } from "react";
import { toast } from 'react-toastify';

import AuthContext from "./authContext";
import authReducer from "./authReducer";
import NotificationSucces from "../../components/notifications/NotificationsSucces";
import NotificationError from "../../components/notifications/NotificationsError";
import { ClientAxios } from "../../services/api.axios";
import { apiRoutes } from "../../services/api.routes";
import { useLogin } from "../../hooks/useLogin";

import {
  LOGIN,
  SIGNUP,
  LIST_USERS,
  ERROR_LOGIN,
  ERROR_SIGNUP,
  ERROR_LIST_USERS,
} from "./types";


const AuthState = ( { children }: any ) => {
  const initialState: ContextType = {
    tokenAuth: "",
    auth: false,
    listUsers: {
      page: null,
      per_page: null,
      total: null,
      total_pages: null,
      data: []
    },
    login: () => {},
    signup: () => {},
    listUser: () => {},
  }

  const [ state, dispatch ] = useReducer(authReducer, initialState);
  const { login: LoginHook} = useLogin();
  const login = async (data: IUser) => {
    try {
      let route = apiRoutes.auth.login()
      const res = await ClientAxios.post(route, data);
      if(res.status === 200){
        LoginHook(res?.data, true)
        toast.success(<NotificationSucces mensaje="Login exitoso" />)
        dispatch({
          type: LOGIN,
          payload: res.data
        })
      }
    } catch (error) {
      toast.error(<NotificationError mensaje="Usuario no encontrado" />)
      dispatch({
        type: ERROR_LOGIN
      })
    }
  }

  const signup = async (data: IUser) => {
    try {
      let route = apiRoutes.auth.signup()
      const res = await ClientAxios.post(route, data);
      if(res.status === 200){
        LoginHook(res?.data?.token, true)
        toast.success(<NotificationSucces mensaje="Usuario creado exitosamente" />)
        dispatch({
          type: SIGNUP,
          payload: res.data.token
        })
      }
    } catch (error) {
      toast.error(<NotificationError mensaje="Usuario no creado" />)
      dispatch({
        type: ERROR_SIGNUP
      })
    }
  }

  const listUser = async (page: number) => {
    try {
      let route = apiRoutes.list.list(page)
      const res = await ClientAxios.get(route);
      if(res.status === 200){
        dispatch({
          type: LIST_USERS,
          payload: res.data
        })
      }
    } catch (error) {
      dispatch({
        type: ERROR_LIST_USERS
      })
      
    }
  }
  
  return (
    <AuthContext.Provider
      value={{
        tokenAuth: state.tokenAuth,
        auth: state.auth,
        listUsers: state.listUsers,
        login,
        signup,
        listUser
      }}
    >
      {children}
    </AuthContext.Provider>
  )
}

export default AuthState;