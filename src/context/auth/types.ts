export const LOGIN = "LOGIN";
export const SIGNUP = "SIGNUP";
export const LIST_USERS = "LIST_USERS";
export const ERROR_LOGIN = "ERROR_LOGIN";
export const ERROR_SIGNUP = "ERROR_SIGNUP";
export const ERROR_LIST_USERS = "ERROR_LIST_USERS";