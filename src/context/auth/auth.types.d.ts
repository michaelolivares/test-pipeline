interface IUser {
  email: string;
  password: string;
}

interface IUserList {
  id: number;
  email: string;
  first_name: string;
  last_name: string;
  avatar: string;
}

interface IListResponse {
  page: number | null;
  per_page: number | null;
  total: number | null;
  total_pages: number | null;
  data: IUserList[]
}

type ContextType = {
  tokenAuth: string;
  auth: boolean;
  listUsers: IListResponse;
  login: (user: IUser) => void;
  signup: (user: IUser) => void;
  listUser: (page: number) => void;
}