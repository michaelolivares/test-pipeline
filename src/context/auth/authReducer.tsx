import {
  LOGIN,
  SIGNUP,
  LIST_USERS,
  ERROR_LOGIN,
  ERROR_SIGNUP,
  ERROR_LIST_USERS,
} from "./types";

const authReducer = ( state: any, action: any) => {
  switch(action.type){
    case LOGIN:
      return {
        ...state,
        tokenAuth: action.payload,
        auth: true,
      }
    case SIGNUP: 
      return {
        ...state,
        auth:true,
        tokenAuth: action.payload,
      }
    case LIST_USERS:
      return {
        ...state,
        listUsers: action.payload,
      }
    case ERROR_LOGIN:
    case ERROR_SIGNUP:
      return {
        ...state,
        auth: false,
        tokenAuth: ""
      }
    case ERROR_LIST_USERS:
      return {
        ...state,
        listUsers: {
          page: null,
          per_page: null,
          total: null,
          total_pages: null,
          data: []
        },
      }
    default:
      return state
  }
}
export default authReducer;