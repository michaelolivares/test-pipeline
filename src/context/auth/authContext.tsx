import { createContext } from 'react';

const contextDefaultValues: ContextType = {
  tokenAuth: "",
  auth: false,
  listUsers: {
    page: null,
    per_page: null,
    total: null,
    total_pages: null,
    data: []
  },
  login: () => {},
  signup: () => {},
  listUser: () => {},
}

const authContext = createContext<ContextType>(contextDefaultValues);
export default authContext;