export type ICredentials = {
  token: string;
}

const getCredentials = () => {
  const credentials = localStorage.getItem("credentials") || sessionStorage.getItem("credencial");
  if(!credentials) return undefined
  return JSON.parse(credentials);
}

const setCredentials = (credentials: ICredentials, remember: boolean) => {
  const storage = remember ? localStorage : sessionStorage;
  storage.setItem("credentials", JSON.stringify(credentials));
}

const clearCredentials = () => {
  localStorage.removeItem("credentials");
  sessionStorage.removeItem("credentials");
}

export const useLogin = () => {
  const credentials = getCredentials();
  const login = setCredentials;
  const logout = clearCredentials;
  const isLogin = Boolean(credentials?.token);
  return { isLogin, credentials, login, logout}
}